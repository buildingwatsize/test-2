from django.db import models


class Item(models.Model):
    amount = models.FloatField(default=0)
