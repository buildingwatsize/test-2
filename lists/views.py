from django.shortcuts import redirect, render
from lists.models import Item


def home_page(request):

    if request.method == 'POST':
        amount = float(request.POST['item_text'])

        Item.objects.create(amount=amount)
        return redirect('/')

    moneys = Item.objects.all()

    MoneysinArr = []
    Sum = 0
    avg = 0
    Max, Min = 0, 0
    Elements = [0]
    revElements = [0]
    Sorter = []

    for i in moneys:
        Sum = Sum+i.amount
        MoneysinArr.append(i.amount)
        MoneysinArr = sorted(MoneysinArr)
        Elements = MoneysinArr[:]
        revElements = MoneysinArr[::-1]

    Min = Elements[0]
    Max = revElements[0]

    for row in zip(Elements, revElements):
        Sorter.append(row)

    if (moneys.count() != 0):
        avg = Sum / moneys.count()
    return render(request, 'home.html',
                  {'items': moneys, 'sum':  Sum, 'average': avg,
                   'Max': Max, 'Min': Min, 'Elements': Elements,
                   'revElements': revElements, 'Sorter': Sorter})
