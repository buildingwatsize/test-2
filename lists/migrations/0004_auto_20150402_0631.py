# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0003_auto_20150402_0621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='amount',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
    ]
