# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0002_item_text'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='text',
        ),
        migrations.AddField(
            model_name='item',
            name='amount',
            field=models.TextField(default='0'),
            preserve_default=True,
        ),
    ]
