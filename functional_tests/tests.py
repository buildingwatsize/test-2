from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_row_in_min_max_sorted_table(self, row_text):
        table = self.browser.find_element_by_id('min_max_sorted_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row_text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)

        self.assertIn('Bank', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Bank', header_text)

        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
                inputbox.get_attribute('placeholder'),
                'ป้อนจำนวนเงิน'
        )
        inputbox.send_keys(10)
        inputbox.send_keys(Keys.ENTER)
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys(230)
        inputbox.send_keys(Keys.ENTER)

        self.check_for_row_in_list_table('ครั้งที่ 2: 230.0 บาท')
        self.check_for_row_in_list_table('ครั้งที่ 1: 10.0 บาท')
        self.check_for_row_in_list_table('ผลรวม : 240.0 บาท')
        self.check_for_row_in_list_table('ค่าเฉลี่ยต่อครั้ง : 120.0 บาท')
        # test min and max value
        self.check_for_row_in_min_max_sorted_table('Min: 10.0 บาท')
        self.check_for_row_in_min_max_sorted_table('Max: 230.0 บาท')
        # test min to max sorting
        self.check_for_row_in_min_max_sorted_table('10.0 230.0 บาท')
        # test max to min sorting
        self.check_for_row_in_min_max_sorted_table('230.0 10.0 บาท')

        self.fail('Finish the test!')
