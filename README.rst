Overview
========

Simple Bank " โปรแกรมคำนวณการหยอดกระปุกออมสิน "


Features
========

- Maximum , Minimum , Summation , Average "สามารถดูค่าผลรวม ค่าสูงสุด ต่ำสุด และค่าเฉลี่ยได้"
- View Sorting Ascending and Descending [Min->Max] [Max->Min] "สามารถดูการเรียงลำดับจากต่ำไปสูงและสูงไปต่ำได้"


Use cases
=========

Maximum , Minimum , Summation , Average "สามารถดูค่าผลรวม ค่าสูงสุด ต่ำสุด และค่าเฉลี่ยได้"
------------

#. go to main page "เข้าหน้าหลัก"
#. enter a value in a textbox "ใส่จำนวนเงินที่ต้องการคำนวณ"
#. press 'Enter' key "กดปุ่ม Enter"


View Sorting Ascending and Descending [Min->Max] [Max->Min] "สามารถดูการเรียงลำดับจากต่ำไปสูงและสูงไปต่ำได้"
----------

#. go to main page "เข้าหน้าหลัก"
#. view result "ผลลัพธ์แสดงผลด้านล่างของช่่องใส่ค่าจำนวนเงิน"